﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//public class Boundary
//{
//	public float xMin, xMax, zMin, zMax;
//}

public class playerController1 : MonoBehaviour
{
	public float speed;
	//public float tilt;
	//public Boundary boundary;

	Rigidbody rigid;
	// Use this for initialization
	void Start()
	{

		//Initialize rigid2D
		rigid = GetComponent<Rigidbody>();
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rigid.velocity = movement * speed;

		//rigid.position = new Vector3 
		///	(
		//	Mathf.Clamp (rigid.position.x, boundary.xMin, boundary.xMax), 
		//	0.0f, 
		//		Mathf.Clamp (rigid.position.z, boundary.zMin, boundary.zMax)
		//	);

		//rigid.rotation = Quaternion.Euler (0.0f, 0.0f, rigid.velocity.x * -tilt);
	}
}